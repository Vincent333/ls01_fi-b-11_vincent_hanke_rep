import java.util.Scanner;


		public class Endlosmodus {

			public static void main(String[] args) {
				Scanner endlos = new Scanner(System.in);
			      
			    double zuZahlenderBetrag; 
			    double eingezahlterGesamtbetrag;
				boolean schleife=true;

				while (schleife==true){
					
					zuZahlenderBetrag = fahrkarte(tarif(endlos));
					zuZahlenderBetrag = ticketAnzahl(endlos, "\nAnzahl der Tickets: ",zuZahlenderBetrag);
				
					
					eingezahlterGesamtbetrag = fahrkartenBezahlen(endlos, zuZahlenderBetrag);
					
					
					fahrkartenAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
				}
			    endlos.close();
			}
			
		    
			public static double fahrkarte(byte ticket){
				switch(ticket){
					case 1:
						return 2.90;
					case 2:
						return 8.60;
					case 3:
						return 23.50;
				}
				return 0;
			}

			
			public static byte tarif(Scanner auswahl) {
				System.out.println("\nWählen Sie Ihr gewünschtes Ticket aus:\n"+
									"  Einzelfahrschein zum Regeltarif von 2,90 EUR  (1)\n"+
									"  Tageskarte zum Regeltarif von 8,60 EUR  (2)\n"+
									"  Kleingruppen-Tageskarte (bis 5 Personen) zum Regeltarif von 23,50 EUR  (3)\n\n");
				byte ticket=0;
				do {
					System.out.print("\nIhre Wahl: ");
					ticket=auswahl.nextByte();
					if (1<=ticket && ticket<=3){
						return ticket;
					}
					else{
						System.out.println("\nIhre Eingabe ist ungültig. Bitte wählen Sie eines der vorgegeben Tickets aus.");
						ticket=0;
					}
				} while (ticket==0);
				return ticket;
			}

			public static double ticketAnzahl(Scanner wrong, String text,double betrag){
				System.out.println(text);
				byte ticket=wrong.nextByte();
				if (1<=ticket && ticket<=10){
					return ticket*betrag;
				}
				else {
					System.out.println("\nIhr Eingabe ist ungültig.");
					return betrag;
				}
			}

			public static double fahrkartenbestellungErfassen(Scanner fine,String text) {
				System.out.println(text);
				return fine.nextDouble();
			}
			
			
			public static double fahrkartenBezahlen(Scanner fine, double zuZahlenderBetrag) {
			    double eingezahlterGesamtbetrag = 0.0;
			    while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			    	System.out.printf("\nNoch zu zahlen: %.2f EURO%n",zuZahlenderBetrag-eingezahlterGesamtbetrag);
					double eingeworfeneMünze = fahrkartenbestellungErfassen(fine , "Eingabe (mind. 5Ct, höchstens 2 Euro): ");				
					if (eingeworfeneMünze<0 || 2<eingeworfeneMünze){
						System.out.println("\nFalsche Eingabe!");
					}
					else{
						eingezahlterGesamtbetrag += eingeworfeneMünze;
					}
			    }	
			    return eingezahlterGesamtbetrag;
			}
			
			
			public static void fahrkartenAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
			    System.out.println("\nFahrschein wird ausgegeben");
			    for (int i = 0; i < 8; i++){
			    	System.out.print("=");
			    	warte(250);
			    }
			    System.out.println("\n\n");
			    rueckgeldAusgabe(eingezahlterGesamtbetrag, zuZahlenderBetrag);	    
			    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
			    				   "vor Fahrtantritt entwerten zu lassen!\n"+
			    				   "Wir wünschen Ihnen eine gute Fahrt.\n");	    
			}
			
			public static void warte(int time) {
				try {
					Thread.sleep(time);
		    		} catch (InterruptedException e) {
		    			e.printStackTrace();
		    		}
			}
			
			
			public static void rueckgeldAusgabe(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
				double rueckgabebetrag;
			    rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
			    if(rueckgabebetrag > 0.0) {
			    	System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO%n",rueckgabebetrag);
			    	System.out.println("wird in folgenden Münzen ausgezahlt:");

			    	while(rueckgabebetrag >= 2.0)
			    	{
			    		muenzeAusgeben(2," Euro");
			    		rueckgabebetrag -= 2.0;
			    	}
			    	while(rueckgabebetrag >= 1.0)
			    	{
			    		muenzeAusgeben(1," Euro");
			    		rueckgabebetrag -= 1.0;
			    	}
			    	while(rueckgabebetrag >= 0.5)
			    	{
			    		muenzeAusgeben(50," Cent");
			    		rueckgabebetrag -= 0.5;
			    	}
			    	while(Math.round(rueckgabebetrag*100.0)/100.0 >= 0.2)
			    	{
			    		muenzeAusgeben(20," Cent");
			    		rueckgabebetrag -= 0.2;
			    	}
			    	while(Math.round(rueckgabebetrag*100.0)/100.0 >= 0.1)
			    	{
			    		muenzeAusgeben(10," Cent");
			    		rueckgabebetrag -= 0.1;
			    	}
			    	while(Math.round(rueckgabebetrag*100.0)/100.0 >= 0.05)
			    	{
			    		muenzeAusgeben(5," Cent");
			    		rueckgabebetrag -= 0.05;
			    	}
			    }
			}
			
			
			public static void muenzeAusgeben(int betrag, String Summe) {
				System.out.println(betrag + Summe);
			}
	}


﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       float zuZahlenderBetrag; 
       float eingezahlterGesamtbetrag;
       float eingeworfeneMünze;
       float rückgabebetrag;
       int anzahlTickets;
       
       System.out.printf("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextFloat();
       
       System.out.printf("Wieviele Tickets werden benötigt? : ");
       anzahlTickets = tastatur.nextInt();
       
       System.out.printf("Dieser Preis entspricht: %.2f EURO", zuZahlenderBetrag*anzahlTickets);
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.00f;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag*anzahlTickets)
       {
    	   System.out.printf("\nNoch zu zahlen: %.2f EURO", zuZahlenderBetrag*anzahlTickets - eingezahlterGesamtbetrag);
    	   System.out.printf("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag*anzahlTickets;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("\nDer Rückgabebetrag in Höhe von %.2f EURO", rückgabebetrag);
    	   System.out.printf("\nwird in folgenden Münzen ausgezahlt: ");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(Math.round(rückgabebetrag) >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       } 

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       tastatur.close(); 
       
    } 
    
    
    
    
}
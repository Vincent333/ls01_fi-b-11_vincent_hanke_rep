
public class Konsoleneingabe {

	public static void main(String[] args) {
		System.out.println("Hello!\nWie geht es dir?");
		System.out.println("Test\bTest");
		System.out.println("Test\tTest");
		System.out.println("Test\fTest");
		System.out.println("Test\rTest");
		System.out.println("Test\"Test\"");
		System.out.println("Test\'Test\'");
		System.out.println("Test\\Test\\");
	
		String z = "Java-Programm";
		int i = 123;
		
		System.out.printf("|%-20.4s|",z);
		System.out.printf("\n%d %d", i, i);
		System.out.printf("|%5d| |%5d|\n" , i, -i);
		System.out.printf( "|%-5d| |%-5d|\n" , i, -i);
		System.out.printf( "|%+-5d| |%+-5d|\n" , i, -i);
		System.out.printf( "|%X| |%x|\n", 0xabc, 0xabc );
		System.out.printf( "|%05d| |%05d|\n\n", i, -i);
		System.out.printf( "|%08x| |%#x|\n\n", 0xabc, 0xabc );
		
	}

}

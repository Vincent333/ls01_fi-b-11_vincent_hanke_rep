import java.util.Scanner;
			//Aufgabe 1 
class Cases
{
    public static void main(String[] args)
    {
       Scanner notenRechner = new Scanner(System.in);
       
       char noten;
       
       System.out.println("Hallo, dieses Programm �bersetzt Ihnen eine Note.");
       System.out.println("Bitte geben Sie eine Zahl von 1-6 ein: ");
       noten = notenRechner.next().charAt(0);
       
       switch(noten) {
       case '1':
    	   System.out.println("Das ist die Note f�r: Sehr gut");
    	   break;
       case '2':
    	   System.out.println("Das ist die Note f�r: Gut");
    	   break;
       case '3':
    	   System.out.println("Das ist die Note f�r: Befreidigend");
    	   break;
       case '4':
    	   System.out.println("Das ist die Note f�r: Ausreichend");
    	   break;
       case '5':
    	   System.out.println("Das ist die Note f�r: Mangelhaft");
    	   break;
       case '6':
    	   System.out.println("Das ist die Note f�r: Ungen�gend");
    	   break;
       default :
    	   System.out.println("Bitte bleiben Sie bei den bekannten Schulnoten, also 1-6");
    	   noten = notenRechner.next().charAt(0);
    	   break;
       }
       
       System.out.println("Vielen Dank f�r das benutzten dieses Programmes.");
       
       notenRechner.close();
       


	}

}
